<?php
/**
 * Created by PhpStorm.
 * User: oksana.repkina
 * Date: 07.09.17
 * Time: 11:15
 */

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction(){
        $design = [
            '1.jpg',
            '2.jpeg',
            '3.jpg',
            '4.jpg',
            '5.jpg',
            '6.jpg',
            '7.jpg',
            '8.jpg',
            '9.jpg',
            '10.jpg',
        ];

        $summer = [
            'summer-1.jpg',
            'summer-2.jpeg',
            'summer-3.jpg',
            'summer-4.jpeg',
            'summer-5.jpg',
            'summer-6.jpg',
            'summer-7.jpeg',
        ];

        $autumn = [
            'autumn-1.jpg',
            'autumn-2.jpg',
            'autumn-3.jpg',
            'autumn-4.jpg',
            'autumn-5.jpg',
            'autumn-6.jpg',
        ];

        $images = array_merge($design, $summer, $autumn);

        shuffle($images);

        $randomisedImages = array_slice($images, 0, 8);

        return $this->render('home/index.html.twig', [
            'randomised_images' => $randomisedImages,
            'design_images' => array_slice($design, 0, 2),
            'summer_images' => array_slice($summer, 0, 2),
            'autumn_images' => array_slice($autumn, 0, 2),
        ]);
    }
}