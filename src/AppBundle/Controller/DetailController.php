<?php
/**
 * Created by PhpStorm.
 * User: oksana.repkina
 * Date: 06.09.17
 * Time: 14:03
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DetailController extends Controller
{
    /**
     * @Route("/view", name="view")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $image = '1.jpg';

        return $this->render('detail/index.html.twig', [
            'image' => $image
        ]);
    }
}